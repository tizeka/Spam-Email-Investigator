import sys
import os
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine


Base = declarative_base()

class Emails(Base):
    __tablename__ = 'emails'
    id = Column(Integer, primary_key=True)
    email = Column(String(250))
    message_id = Column(String(40))
    domain_url = Column(String(40))
    ip_address = Column(String(15))
    status_code = Column(Integer)
    hosting_contact = Column(String(50))
    registrar_contact = Column(String(50))
    date_sent = Column(DateTime)

engine = create_engine('sqlite:///email_database.db')

Base.metadata.create_all(engine)
